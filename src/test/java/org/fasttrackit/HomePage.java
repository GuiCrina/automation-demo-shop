package org.fasttrackit;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;

    public class HomePage {

        private final SelenideElement greetings = $(".navbar-text > span");
        private final SelenideElement logo = $(".fa-shopping-bag");
        private final SelenideElement cartIcon = $(".fa-shopping-cart");
        private final SelenideElement cartRedirectUrl = $("[data-icon=shopping-cart]");
        private final SelenideElement helpModal = $(".fa-question");
        private final SelenideElement getCartRedirectUrl = $("data-icon= shopping-cart");
        private final SelenideElement loginModal = $(" modal content");
        private final SelenideElement buildDemoShop = $(".nav-link");
        private final SelenideElement isFavoritesIconDisplayed = $("");
        private final SelenideElement AWESOME_GRANITE_CHIPS_PRODUCT = $(".text-center card-body");

        public void clickOnLogo(){
            logo.click();
        }

        public String greetingsMessage() {
            return greetings.getText();
        }

        public String logoRedirectURL() {
            return logo.parent().getAttribute("href");
        }

        public boolean isLogoDisplayed() {
            return logo.exists();
        }

        public boolean DemoShop() {
            return loginModal.exists();
        }

        public boolean isCartIconDisplayed() {
            return cartIcon.exists();
        }

        public String logoRedirectUrl() {
            return logo.parent().getAttribute("href");
        }

        public String cartRedirectUrl() {
            return cartRedirectUrl.getAttribute("href");
        }

        public boolean helpModal() {
            return helpModal.exists();
        }

        public boolean loginModal() {
            return loginModal.exists();
        }
    }




