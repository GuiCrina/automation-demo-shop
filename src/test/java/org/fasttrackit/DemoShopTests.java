package org.fasttrackit;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import static com.codeborne.selenide.Selenide.*;
import static com.codeborne.selenide.Selenide.$;
import static org.testng.Assert.*;
import static org.testng.Assert.assertTrue;


public class DemoShopTests {

    public static final String HOME_PAGE = "https://fasttrackit-test.netlify.app/";
    //private static final HomePage homePage = new HomePage();
    HomePage homePage;

    @BeforeTest
    public void openPage() {
        open(HOME_PAGE);
        //  HomePage homePage = new HomePage();
        HomePage homePage = new HomePage();

    }

    @Test
    public void validate_homePage_logo() {
        HomePage homePage = new HomePage();
        assertTrue(homePage.isLogoDisplayed(), "Logo element exist in navigation bar");
        assertEquals(homePage.logoRedirectUrl(), HOME_PAGE, "Clicking the Logo redirects to homepage");
    }

    @Test
    public void cart_icon() {
        SelenideElement greetings = $(".navbar-text > span");
        SelenideElement logo = $(".fa-shopping-bag");
        SelenideElement cartIcon = $(".fa-shopping-cart");
        SelenideElement cartRedirectUrl = $("[data-icon=shopping-cart]");
        SelenideElement helpModal = $(".fa-question");
    }

    @Test
    public void verify_date_build_demo_shop() {
        SelenideElement buildDemoShop = $(".nav-link");
        String buildVersion = buildDemoShop.getText();
        assertTrue(buildDemoShop.exists(), "Check if the site creation button appears on the site");
        assertTrue(buildDemoShop.isDisplayed());
        assertEquals(buildVersion, "Demo Shop | build date 2021-05-21 14:04:30 GTBDT");

    }

    @Test
    public void validate_greetings_message() {
        HomePage homePage = new HomePage();
        String message = homePage.greetingsMessage();
        assertEquals(message, "Hello guest!");

    }

    @Test
    public void favourites_icon_updates_upon_adding_a_product_to_the_favourites2() {
        SelenideElement secondProduct = $("[href='#/product/3']");
        SelenideElement mainProductCard = secondProduct.parent().parent();
        SelenideElement favouritesButton = mainProductCard.$("[data-icon*=heart]");
        favouritesButton.click();
        assertEquals(favouritesButton.getAttribute("data-icon"), "heart-broken",
                "After adding a product tot favourites," +
                        "the favourites icon crashes");
        SelenideElement favouritesIcon = $("[data-icon=heart]~.shopping_cart_badge");
        assertEquals(favouritesIcon.getText(), "1",
                "After adding a product to favourites the favourites badges updates");

    }

    @Test
    public void validate_login_button_with_correct_user_and_correct_password() {
        SelenideElement loginButton = $(".fa-sign-in-alt");
        assertTrue(loginButton.exists(), "Successfully login");
        loginButton.click();
        assertTrue(loginButton.isDisplayed(), "After login with correct user and password you will successfully login");
        SelenideElement close = $(".close");
        close.click();
    }

    @Test
    public void verify_the_number_with_the_products_added_to_favourites() {
        SelenideElement verifyNumber = $(".shopping-cart-icon");
        assertTrue(verifyNumber.exists(), "Check the number of products added to favourites");
        verifyNumber.click();
        assertTrue(verifyNumber.isDisplayed(), ".shopping-cart-icon fa-layers fa-fw");
        SelenideElement close = $(".close");
        //close.click();
    }

    @Test
    public void validate_login_button_opens_login_modal() {
        SelenideElement loginIcon = $(".fa-sign-in-alt");
        assertTrue(loginIcon.exists(), "Login icon is displayed");
        SelenideElement loginModal = $(".modal-content");
        assertTrue(loginModal.isDisplayed(), "After clicking on login button,login modal is displayed");
        assertTrue(loginModal.isDisplayed(), "Login Modal is displayed");
        SelenideElement close = $(".close");
        close.click();
    }

    @Test
    public void verify_favourites_icon() {
        SelenideElement favouritesIcon = $(".navbar-nav [data-icon=heart]");
        assertTrue(favouritesIcon.exists(), "Favourites icon is displayed");

    }

    @Test
    public void verify_favourites_button() {
        SelenideElement favouritesIcon = $(".navbar-nav [data-icon=heart]");
        assertTrue(favouritesIcon.exists(), "Favourites icon is displayed");
        favouritesIcon.click();
        String wishListURL = favouritesIcon.parent().getAttribute("href");
        assertEquals(wishListURL, HOME_PAGE + "#/wishlist",
                "Page redirected to WishList Page");
    }

    @Test
    public void verify_cartIcon() {
        SelenideElement cartIcon = $(".shopping-cart-icon");
        assertTrue(cartIcon.exists(), "Functional cart button");
        assertTrue(cartIcon.isDisplayed(), "Cart Icon is displayed on the page");
    }

    @Test
    public void validate_PressIcon() {
        SelenideElement cartIcon = $(".shopping-cart-icon ");
        assertFalse(cartIcon.isSelected(), "Cart button is selected");
        cartIcon.click();
    }

    @Test
    public void get_cart_redirect_URL() {
        SelenideElement getCartRedirectURL = $(".shopping-cart-icon fa-layers fa-fw");
        assertFalse(getCartRedirectURL.isDisplayed(), "Cart is redirecting URL");
        getCartRedirectURL.exists();
        SelenideElement close = $(".close");

    }

    @Test
    public void validate_loginButton_opens_login_modal() {
        SelenideElement loginIcon = $(".fa-sign-in-alt");
        assertTrue(loginIcon.isDisplayed(), "Login icon is displayed");
        loginIcon.click();
        SelenideElement loginModal = $(".modal-content");
        assertTrue(loginModal.isDisplayed(), "After clicking on login button,login modal is displayed");
    }

    @Test
    public void verify_login_button_opens_login_modal() {
        SelenideElement loginIcon = $(".fa-sign-in-alt");
        assertTrue(loginIcon.exists(), "Login icon is displayed");
        loginIcon.click();
        SelenideElement loginModal = $(".modal-content");
        assertTrue(loginModal.exists(),
                "After clicking on login button, login modal is displayed");
        assertTrue(loginModal.isDisplayed(), "Login Modal is displayed");
        SelenideElement close = $(".close");
        close.click();
    }

    @Test
    public void verify_login_button_with_incorrect_user_and_correct_password() {
        SelenideElement loginButton = $(".fa-sign-in-alt");
        assertTrue(loginButton.exists(), "Incorrect user is used");
        loginButton.click();
        SelenideElement loginModal = $(".modal-content");
        assertTrue(loginModal.isDisplayed(), "Login modal is displayed");
        SelenideElement submitLogin = $("button");
        SelenideElement close = $(".close");
        close.click();
    }
}


