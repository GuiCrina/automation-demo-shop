# automation-demo-shop

A brief presentation of my final project.

## This is the final project for Crina, within the FastTrackIt Test Automation course.

Software engineer: Crina Mariana Gui

###Tech stack used:

  - Java17
  - Maven
  - Selenide Framework
  - PageObject Models

### How to run the tests
 - git clone https://gitlab.com/GuiCrina/automation-demo-shop.git
 - Execute the following commands to: - "mvn clean tests"
                                      - "mvn allure:report"
                                      - "mvn allure:serve"
```
cd existing_repo
git remote add origin https://gitlab.com/GuiCrina/automation-demo-shop.git
git branch -M main
git push -uf origin master

###Page Objects:
- DemoShopTest
- -Header
- HomePage

###Tests Implemented:

      Test name                  | Execution |         Date
  
  1.Home page logo                   PASSED             08.04.2022 
  2.Cart icon                        PASSED             08.04.2022
  3.Image of the product             FAILED             08.04.2022
  4.Favourites icon                  FAILED             08.04.2022
  5.Cart redirect Url                PASSED             08.04.2022
  6.Validate press icon              PASSED             08.04.2022
  7.Greetings message                PASSED             08.04.2022
  8.Home page                        PASSED             08.04.2022
  9.Login button                     PASSED             08.04.2022
  10.Login button with correct user  FAILED             08.04.2022
  11.Sort mode products              PASSED             11.04.2022
  12.Login modal                     PASSED             11.04.2022
  13.Date build Demo Shop            PASSED             11.04.2022
  14.Products number added to cart   PASSED             11.04.2022
  15.Products added to favourites    PASSED             11.04.2022
  16.Login button open login modal   PASSED             11.04.2022


  ### Reporting is also available in the Allure reports folder.
  ### Tests are executed using Maven.


|## Collaborate with my team

- [ ] [Invite team members and collaborators](https://docs.gitlab.com/ee/user/project/members/)
- [ ] [Create a new merge request](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
- [ ] [Automatically close issues from merge requests](https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
- [ ] [Enable merge request approvals](https://docs.gitlab.com/ee/user/project/merge_requests/approvals/)
- [ ] [Automatically merge when pipeline succeeds](https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html)
Members of a project | GitLab
Creating merge requests | GitLab
Merge request approvals | GitLab
Merge when pipeline succeeds | GitLab
Manage issues | GitLab


